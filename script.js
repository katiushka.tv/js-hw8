'use strict'
/* Завдання
Код для завдань лежить в папці project.

Знайти всі параграфи на сторінці та встановити колір фону #ff0000

Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та
вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

Встановіть в якості контента елемента з класом testParagraph наступний параграф -
This is a paragraph

Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному
з елементів присвоїти новий клас nav-item.

Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*/

const par = document.querySelectorAll("p");
par.forEach(elem =>
elem.style.backgroundColor = "#ff0000");

let opt = document.querySelector("#optionsList");
console.log(opt);
console.log(opt.parentNode);
console.log(opt.childNodes);

document.querySelector("#testParagraph").innerText = "This is a paragraph";

for (let el of document.querySelector(".main-header").children) {
    console.log(el);
    el.classList.add("nav-item");
}

document.querySelectorAll(".section-title").forEach(el =>
    el.classList.remove("section-title"));